//------------------------------------------------------------------------------
//
// Original source code: https://github.com/GlyphXTools/alo-viewer
//
//------------------------------------------------------------------------------

#ifndef MODELS_H
#define MODELS_H

#include <QList>
#include <QMatrix4x4>

#include "ChunkFile.hpp"
#include "gameTypes.hpp"

namespace Alamo
{

class Model
{
public:
	enum Chunk
	{
		ChunkSkeleton               = 0x200,
		ChunkSkeletonSize           = 0x201,
		ChunkSkeletonBone           = 0x202,
		ChunkSkeletonBoneName       = 0x203,
		ChunkSkeletonBoneBase       = 0x205,
		ChunkSkeletonBoneAdvanced   = 0x206,
		ChunkMesh                   = 0x400,
		ChunkMeshName               = 0x401,
		ChunkMeshInformation        = 0x402,
		ChunkConnection             = 0x600,
		ChunkConnectionSize         = 0x601,
		ChunkConnectionInformation  = 0x602,
		ChunkConnectionProxy        = 0x603,
		ChunkSubMesh                = 0x10000,
		ChunkSubMeshSize            = 0x10001,
		ChunkSubMeshVertexFormat    = 0x10002,
		ChunkSubMeshVertexData      = 0x10007,
		ChunkSubMeshIndexData       = 0x10004,
		ChunkSubMeshSkinData        = 0x10006,
		ChunkSubMeshCollisionTree   = 0x1200,
		ChunkSubMeshCollisionTree0  = 0x1201,
		ChunkSubMeshCollisionTree1  = 0x1202,
		ChunkSubMeshCollisionTree2  = 0x1203,
		ChunkShader                 = 0x10100,
		ChunkShaderName             = 0x10101,
		ChunkShaderParameterInt     = 0x10102,
		ChunkShaderParameterFloat   = 0x10103,
		ChunkShaderParameterFloat3  = 0x10104,
		ChunkShaderParameterTexture = 0x10105,
		ChunkShaderParameterFloat4  = 0x10106,
		ChunkLight                  = 0x1300,
		ChunkLightName              = 0x1301,
		ChunkLightInformation       = 0x1302,
		ChunkEnd                    = -1
	};

	struct Bone
	{
		int           index;
		int           parent{0};
		QByteArray    name;
		bool          visible{true};
		Billboard billboard{Disable};
		QMatrix4x4    absoluteTransform;
		QMatrix4x4    invertedAbsoluteTransform;
		QMatrix4x4    relativeTransform;
	};
	struct SubMesh
	{
		int                    index;
		int                    mesh;
		QByteArray             shader;
		QList<ShaderParameter> parameters;
		QByteArray             vertexFormat;
		QList<MASTER_VERTEX>   vertices;
		QList<uint16_t>        indices;
		unsigned int           nSkinBones;
		unsigned long          skin[MAX_NUM_SKIN_BONES];
		QByteArray             collisionTree[3];
	};
	struct Mesh
	{
		int            index;
		int            bone;
		QByteArray     name;
		BoundingBox    bounds;
		bool           visible;
		bool           collidable;
		QList<SubMesh> subMeshes;
	};
	struct Light
	{
		int        bone;
		QByteArray name;
		LightType  type;
		QColor     color;
		float      intensity;
		float      farAttenuationEnd;
		float      farAttenuationStart;
		float      hotspotSize;
		float      falloffSize;
	};
	struct Connection
	{
		int object;
		int bone;
	};
	struct Proxy
	{
		int        bone{0};
		QByteArray name;
		bool       visible{true};
		bool       altDecreaseStayHidden{false};
	};

	Model(QFileInfo const &file);
	~Model() = default;

	inline QString const &name() const
	{
		return m_name;
	}

	inline QList<Bone> const &bones() const
	{
		return m_bones;
	}

	inline QList<Bone> &bones()
	{
		return m_bones;
	}

	inline void setDeletedBones(QList<int> const &values)
	{
		m_deletedBones = values;
	}

	void recalculateAbsoluteTransforms();
	void recalculateRelativeTransforms();

	inline QList<Mesh> const &meshes() const
	{
		return m_meshes;
	}

	inline QList<Mesh> &meshes()
	{
		return m_meshes;
	}

	inline QList<int> const &deletedMeshes() const
	{
		return m_deletedMeshes;
	}

	inline void setDeletedMeshes(QList<int> const &values)
	{
		m_deletedMeshes = values;
	}

	inline QList<Light> const &lights() const
	{
		return m_lights;
	}

	inline QList<Light> &lights()
	{
		return m_lights;
	}

	inline QList<Proxy> const &proxies() const
	{
		return m_proxies;
	}

	inline QList<Proxy> &proxies()
	{
		return m_proxies;
	}

	inline void setDeletedProxies(QList<int> const &values)
	{
		m_deletedProxies = values;
	}

	inline QList<Connection> &connections()
	{
		return m_connections;
	}

	inline QList<Connection> const &connections() const
	{
		return m_connections;
	}

	QList<int> computeBoneIndexes() const;
	QList<int> computeObjectIndexes() const;

	void save(QFileInfo const &file, double scale = 1);

	constexpr QString const &error() const
	{
		return m_error;
	}

private:
	void readBone(ChunkFile &reader, Bone &bone);
	void readSkeleton(ChunkFile &reader);
	void readSubMesh(ChunkFile &reader, SubMesh &subMesh);
	void readMesh(ChunkFile &reader, Mesh &mesh);
	void readLight(ChunkFile &reader, Light &light);
	void readConnections(ChunkFile &reader);

	void writeBone(ChunkFile &writer, Bone const &bone) const;
	void writeSkeleton(ChunkFile &writer, double scale) const;
	void writeSubMesh(ChunkFile &writer, SubMesh const &mesh) const;
	void writeMesh(ChunkFile &writer, Mesh const &mesh, double scale) const;
	void writeLight(ChunkFile &writer, Light const &light) const;
	void writeConnections(ChunkFile &writer) const;

	QString           m_name;
	QList<Bone>       m_bones;
	QList<int>        m_deletedBones;
	QList<Mesh>       m_meshes;
	QList<int>        m_deletedMeshes;
	QList<Light>      m_lights;
	QList<Proxy>      m_proxies;
	QList<int>        m_deletedProxies;
	QList<Connection> m_connections;
	QString           m_error;
};

}

#endif
