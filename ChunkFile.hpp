//------------------------------------------------------------------------------
//
// Original source code: https://github.com/GlyphXTools/alo-viewer
//
//------------------------------------------------------------------------------

#ifndef ASSETS_CHUNKFILE_H
#define ASSETS_CHUNKFILE_H

#include <QFile>
#include <QFileInfo>
#include <QVector4D>

namespace Alamo
{

using ChunkType = long;

enum ChunkTypeEnum
{
	Skeleton               = 0x0200,
	SkeletonSize           = 0x0201,
	SkeletonBone           = 0x0202,
	SkeletonBoneName       = 0x0203,
	SkeletonBoneBase       = 0x0205,
	SkeletonBoneBillboard  = 0x0206,
	Mesh                   = 0x0400,
	MeshName               = 0x0401,
	MeshInfo               = 0x0402,
	Connection             = 0x0600,
	SubMesh                = 0x10000,
	SubMeshSize            = 0x10001,
	SubMeshVertexFormat    = 0x10002,
	SubMeshVertexData      = 0x10007,
	SubMeshIndexData       = 0x10004,
	Shader                 = 0x10100,
	ShaderName             = 0x10101,
	ShaderParameterInt     = 0x10102,
	ShaderParameterFloat   = 0x10103,
	ShaderParameterFloat3  = 0x10104,
	ShaderParameterTexture = 0x10105,
	ShaderParameterFloat4  = 0x10106,
	Light                  = 0x1300,
	LightName              = 0x1301,
	LightInfo              = 0x1302,
	End                    = -1
};

struct ChunkHeader
{
	unsigned long type;
	unsigned long size;
};

struct MiniChunkHeader
{
	unsigned char type;
	unsigned char size;
};

class ChunkFile
{
public:
	ChunkFile(QFileInfo const &file, QIODevice::OpenMode mode = QIODevice::ReadOnly);

	ChunkType next();
	ChunkType nextMini();
	void      skip();
	size_t    size();

	size_t readByte(char *buffer, qint64 length, bool check = true);

	constexpr qint64 tell() const
	{
		return m_position;
	}

	inline qint64 fileTell() const
	{
		return m_file.pos();
	}

	constexpr bool group() const
	{
		return m_size < 0;
	}

	constexpr ChunkType type() const
	{
		return m_type;
	}

	QByteArray     readAll(int offset = 0);
	float          readFloat();
	unsigned char  readByte();
	unsigned short readShort();
	unsigned long  readInteger();
	QByteArray     readString();
	QString        readWideString();
	QVector3D      readVector3();
	QVector4D      readVector4();
	QColor         readColorRGB();
	QColor         readColorRGBA();

	template<typename Type>
	inline size_t read(Type *buffer, size_t length, bool check = true)
	{
		return readByte(reinterpret_cast<char *>(buffer), length, check);
	}

	template<typename Value>
	QVector<Value> readData(int size)
	{
		QVector<Value> vector;

		vector.resize(size);
		read(vector.data(), vector.size() * sizeof(Value));
		return vector;
	};

	void beginChunk(ChunkType type);
	void beginMiniChunk(ChunkType type);
	void endChunk();

	void writeRaw(QByteArray const &data);
	void writeByte(char const *buffer, qint64 size);
	void writeShort(unsigned short value);
	void writeFloat(float value);
	void writeInteger(unsigned long value);
	void writeString(QByteArray const &string);
	void writeVector3(QVector3D const &vector);
	void writeVector4(QVector4D const &vector);
	void writeColorRGB(QColor const &color);

	template<typename Type>
	inline void write(Type const *buffer, size_t length)
	{
		return writeByte(reinterpret_cast<char const *>(buffer), length);
	}

	static inline void verify(bool expression)
	{
		if (!expression)
			throw std::runtime_error("Invalid or corrupt file");
	}

	int m_depth;

private:
	template<typename Header>
	struct ChunkInfo
	{
		Header        header;
		unsigned long offset;
	};

	static int const s_maximumDepth{256};

	QFile                      m_file;
	qint64                     m_position;
	long                       m_size{-1};
	qint64                     m_offsets[s_maximumDepth];
	long                       m_miniSize{-1};
	qint64                     m_miniOffset;
	ChunkType                  m_type;
	ChunkInfo<ChunkHeader>     m_chunks[s_maximumDepth];
	ChunkInfo<MiniChunkHeader> m_miniChunk;
};

}
#endif
