//------------------------------------------------------------------------------
//
// Original source code: https://github.com/GlyphXTools/alo-viewer
//
//------------------------------------------------------------------------------

#include <QColor>

#include "ChunkFile.hpp"

using namespace Alamo;
using namespace std;

#pragma pack(1)
struct ChunckHeader
{
	unsigned long type;
	unsigned long size;
};

struct MiniChunckHeader
{
	unsigned char type;
	unsigned char size;
};
#pragma pack()

static uint32_t letohl(uint32_t value)
{
	return (((uint32_t)((uint8_t*)&value)[3]) << 24) | (((uint32_t)((uint8_t*)&value)[2]) << 16) |
		   (((uint32_t)((uint8_t*)&value)[1]) << 8) | (((uint32_t)((uint8_t*)&value)[0]) << 0);
}

static uint16_t letohs(uint16_t value)
{
	return (((uint16_t)((uint8_t*)&value)[1]) << 8) | (((uint16_t)((uint8_t*)&value)[0]) << 0);
}

inline uint32_t htolel(uint32_t value)
{
	uint32_t tmp;
	((uint8_t*)&tmp)[0] = (uint8_t)(value >> 0);
	((uint8_t*)&tmp)[1] = (uint8_t)(value >> 8);
	((uint8_t*)&tmp)[2] = (uint8_t)(value >> 16);
	((uint8_t*)&tmp)[3] = (uint8_t)(value >> 24);
	return tmp;
}

inline uint16_t htoles(uint16_t value)
{
	uint16_t tmp;
	((uint8_t*)&tmp)[0] = (uint8_t)(value >> 0);
	((uint8_t*)&tmp)[1] = (uint8_t)(value >> 8);
	return tmp;
}

ChunkFile::ChunkFile(QFileInfo const &file, QIODevice::OpenMode mode): m_file(file.absoluteFilePath())
{
	m_file.open(mode);
	m_offsets[0] = m_file.size();
	if (mode == QIODevice::ReadOnly)
		m_depth = 0;
	else if (mode == QIODevice::WriteOnly)
		m_depth = -1;
	m_miniChunk.offset = -1;
}

ChunkType ChunkFile::nextMini()
{
	assert(m_depth >= 0);
	assert(m_size >= 0);
	if (m_miniSize >= 0)
		skip(); // We're in a mini chunk, so skip it
	if (m_file.pos() == static_cast<unsigned long>(m_offsets[m_depth])) // We're at the end of the current chunk, move up one
	{
		--m_depth;
		m_size = -1;
		m_position = 0;
		m_type = -1;
	}
	else
	{
		MiniChunckHeader header;

		if (m_file.read(reinterpret_cast<char *>(&header), sizeof(MiniChunckHeader)) != sizeof(MiniChunckHeader))
			throw std::runtime_error("Unable to read file");
		m_miniSize = letohl(header.size);
		m_miniOffset = m_file.pos() + m_miniSize;
		m_position = 0;
		m_type = letohl(header.type);
	}
	return m_type;
}

ChunkType ChunkFile::next()
{
	assert(m_depth >= 0);
	if (m_size >= 0)
		skip(); // We're in a data chunk, so skip it
	if (m_file.pos() == static_cast<unsigned long>(m_offsets[m_depth])) // We're at the end of the current chunk, move up one
	{
		--m_depth;
		m_size = -1;
		m_position = 0;
		m_type = -1;
	}
	else
	{
		ChunckHeader  header;
		unsigned long size;

		if (m_file.read(reinterpret_cast<char *>(&header), sizeof(ChunckHeader)) != sizeof(ChunckHeader))
			throw std::runtime_error("Unable to read file");
		size = letohl(header.size);
		m_offsets[++m_depth] = m_file.pos() + (size & 0x7FFFFFFF);
		m_size = (~size & 0x80000000) ? size : -1;
		m_miniSize = -1;
		m_position = 0;
		m_type = letohl(header.type);
	}
	return m_type;
}

void ChunkFile::skip()
{
	if (m_miniSize >= 0)
		m_file.seek(m_miniOffset);
	else
	{
		m_file.seek(m_offsets[m_depth--]);
		m_size = -1;
		m_position = 0;
	}
}

size_t ChunkFile::size()
{
	return (m_miniSize >= 0) ? m_miniSize : m_size;
}

size_t ChunkFile::readByte(char *buffer, qint64 length, bool check)
{
	if (m_size >= 0)
	{
		auto result{m_file.read(buffer, qMin(static_cast<size_t>(m_position + length), size()) - m_position)};

		m_position += result;
		if (check && result != length)
			throw std::runtime_error("Unable to read file");
		return length;
	}
	throw std::runtime_error("Unable to read file");
	return -1;
}

QByteArray ChunkFile::readAll(int offset)
{
	m_file.seek(m_file.pos() - offset);
	return m_file.readAll();
}

QByteArray ChunkFile::readString()
{
	QByteArray data{static_cast<qsizetype>(size() / sizeof(char)), '\0'};

	read(data.data(), size());
	data.chop(1);
	return data;
}

QString ChunkFile::readWideString()
{
	QVector<wchar_t> data;

	data.resize(size() / sizeof(wchar_t));
	read(data.data(), size());
	return QString::fromWCharArray(data.data());
}

float ChunkFile::readFloat()
{
	float value{0.F};

	if (read(&value, sizeof(value)) < sizeof(value))
		throw std::runtime_error("Unable to read file");
	return value;
}

QVector3D ChunkFile::readVector3()
{
	return {readFloat(), readFloat(), readFloat()};
}

QVector4D ChunkFile::readVector4()
{
	QVector4D out;

	out.setX(readFloat());
	out.setY(readFloat());
	out.setZ(readFloat());
	out.setW(readFloat());
	return out;
}

QColor ChunkFile::readColorRGB()
{
	return QColor::fromRgbF(readFloat(), readFloat(), readFloat());
}

QColor ChunkFile::readColorRGBA()
{
	return QColor::fromRgbF(readFloat(), readFloat(), readFloat(), readFloat());;
}

unsigned char ChunkFile::readByte()
{
	uint8_t value{0};

	if (read(&value, sizeof(value)) < sizeof(value))
		throw std::runtime_error("Unable to read file");
	return value;
}

unsigned short ChunkFile::readShort()
{
	uint16_t value{0};

	if (read(&value, sizeof(value)) < sizeof(value))
		throw std::runtime_error("Unable to read file");
	return letohs(value);
}

unsigned long ChunkFile::readInteger()
{
	uint32_t value{0};

	if (read(&value, sizeof(value)) < sizeof(value))
		throw std::runtime_error("Unable to read file");
	return letohl(value);
}

void ChunkFile::beginChunk(ChunkType type)
{
	ChunckHeader header{0, 0};

	++m_depth;
	m_chunks[m_depth].offset = m_file.pos();
	m_chunks[m_depth].header.type = type;
	m_chunks[m_depth].header.size = 0;
	if (m_depth > 0)
		m_chunks[m_depth - 1].header.size |= 0x80000000; // Set 'container' bit in parent chunk
	m_file.write(reinterpret_cast<char *>(&header), sizeof(ChunckHeader)); // Write dummy header
}

void ChunkFile::beginMiniChunk(ChunkType type)
{
	MiniChunckHeader header = {0, 0};

	assert(m_depth >= 0);
	assert(m_miniChunk.offset == static_cast<unsigned long>(-1));
	assert(type <= 0xFF);
	m_miniChunk.offset = m_file.pos();
	m_miniChunk.header.type = (uint8_t)type;
	m_miniChunk.header.size = 0;
	m_file.write(reinterpret_cast<char *>(&header), sizeof(MiniChunckHeader)); // Write dummy header
}

void ChunkFile::endChunk()
{
	auto position{m_file.pos()};

	assert(m_depth >= 0);
	if (m_miniChunk.offset != static_cast<unsigned long>(-1))
	{
		// Ending mini-chunk
		long             size{static_cast<long>(position - (m_miniChunk.offset + sizeof(MiniChunckHeader)))};
		MiniChunckHeader header{m_miniChunk.header.type, static_cast<uint8_t>(size)};

		assert(size <= 0xFF);
		m_file.seek(m_miniChunk.offset);
		m_file.write(reinterpret_cast<char *>(&header), sizeof(MiniChunckHeader));
		m_miniChunk.offset = -1;
	}
	else
	{
		// Ending normal chunk
		long size{static_cast<long>(position - (m_chunks[m_depth].offset + sizeof(ChunckHeader)))};

		m_chunks[m_depth].header.size = (m_chunks[m_depth].header.size & 0x80000000) | (size & ~0x80000000);

		ChunckHeader header{htolel(m_chunks[m_depth].header.type), htolel(m_chunks[m_depth].header.size)};

		m_file.seek(m_chunks[m_depth].offset);
		m_file.write(reinterpret_cast<char *>(&header), sizeof(ChunckHeader));
		--m_depth;
	}
	m_file.seek(position);
}

void ChunkFile::writeRaw(QByteArray const &data)
{
	m_file.write(data);
}

void ChunkFile::writeByte(char const *buffer, qint64 size)
{
	assert(m_depth >= 0);
	if (m_file.write(buffer, size) != size)
		throw std::runtime_error("Unable to write file");
}

void ChunkFile::writeShort(unsigned short value)
{
	uint16_t data{htoles(value)};

	write(&data, sizeof(uint16_t));
}

void ChunkFile::writeFloat(float value)
{
	write(&value, sizeof(float));
}

void ChunkFile::writeInteger(unsigned long value)
{
	uint32_t data{htolel(value)};

	write(&data, sizeof(unsigned long));
}

void ChunkFile::writeString(QByteArray const &string)
{
	write(string.data(), string.size() + 1);
}

void ChunkFile::writeVector3(QVector3D const &vector)
{
	writeFloat(vector.x());
	writeFloat(vector.y());
	writeFloat(vector.z());
}

void ChunkFile::writeVector4(QVector4D const &vector)
{
	writeFloat(vector.x());
	writeFloat(vector.y());
	writeFloat(vector.z());
	writeFloat(vector.w());
}

void ChunkFile::writeColorRGB(QColor const &color)
{
	writeFloat(color.redF());
	writeFloat(color.greenF());
	writeFloat(color.blueF());
}
