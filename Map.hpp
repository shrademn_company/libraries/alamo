//------------------------------------------------------------------------------
//
// Original source code: https://github.com/AlamoEngine-Tools/ModCheck
//
//------------------------------------------------------------------------------

#ifndef ASSETS_MAP_H
#define ASSETS_MAP_H

#include <QFileInfo>
#include <QList>
#include <QPointF>
#include <QSizeF>

#include "gameTypes.hpp"

namespace Alamo
{

class ChunkFile;

enum MapType
{
	MAP_LAND  = 1,
	MAP_SPACE = 2
};

class Map
{
public:
	enum Chunk
	{
		ChunkPreview                 = 0x13,
		ChunkEnvironment             = 0x100,
		ChunkTerrain                 = 0x101,
		ChunkTerrainLayer            = 3,
		ChunkTerrainDecal            = 0x10,
		ChunkTerrainTrack            = 0x100,
		ChunkTerrainTrackInformation = 0x101,
		ChunkTerrainTrackTexture     = 0x103,
		ChunkTerrainTrackPoints      = 0x102,
		ChunkTerrainTrackWidths      = 0x104,
		ChunkTerrainTrackOpacities   = 0x105,
		ChunkPassability             = 0x10A,
		ChunkObject                  = 0x102,
		ChunkZoneInformation         = 0x103,
		ChunkZoneData                = 0x10B,
		ChunkEnd                     = -1
	};

	struct Properties
	{
		int           game;
		unsigned long players;
		MapType       type;
		QString       name;
		QString       planet;
		QSizeF        dimension;
	};
	struct Water
	{
		QByteArray maps[3];
	};
	struct Layer
	{
		QByteArray colorTexture;
		QByteArray normalTexture;
	};
	struct Environment
	{
		qint64     position;
		quint64    size;
		QByteArray name;
		QByteArray scenario;
		QByteArray skybox1;
		QByteArray skybox2;
		QByteArray clouds;
	};
	struct Track
	{
		QByteArray m_texture;
	};
	struct Object
	{
		unsigned long crc;              // CRC of the object type name
		unsigned long initialObject;    // Initial object that is placed on this object
	};
	struct Data
	{
		QSize      size{0, 0};
		QList<int> elevationMap;
		int        maximumElevation{std::numeric_limits<qint16>::min()};
		int        minimumElevation{std::numeric_limits<qint16>::max()};
		QList<int> passabilityMap;
		QList<int> zoneMap;
		QPointF    playableTop;
		QPointF    playableBottom;
	};

	Map(QFileInfo const &file);
	Map(Map const &map) = default;
	~Map() = default;

	constexpr QFileInfo const &file() const
	{
		return m_file;
	}

	constexpr Properties const &properties() const
	{
		return m_properties;
	}

	constexpr Water const &water() const
	{
		return m_water;
	}

	constexpr Data const &heightmap() const
	{
		return m_data;
	}

	constexpr QList<Layer> const &layers() const
	{
		return m_layers;
	}

	constexpr QList<Environment> const &environments() const
	{
		return m_environments;
	}

	inline const QList<Track>&tracks() const
	{
		return m_tracks;
	}

	constexpr QList<Object> const &objects() const
	{
		return m_objects;
	}

	constexpr bool preview() const
	{
		return m_preview;
	}

	constexpr QString const &ai() const
	{
		return m_ai;
	}

	constexpr QString const &error() const
	{
		return m_error;
	}

private:
	void readProperties(ChunkFile &reader);
	void readEnvironments(ChunkFile &reader);
	void readLayer(ChunkFile &reader, Layer &layer);
	void readDecals(ChunkFile &reader);
	void readTracks(ChunkFile &reader);
	void readScrollInfo(ChunkFile &reader);
	void readTerrain(ChunkFile &reader);
	void readPassability(ChunkFile &reader);
	void readObject(ChunkFile &reader);
	void readObjects(ChunkFile &reader);
	void readZones(ChunkFile &reader);

	QMap<QPair<int, int>, QList<int>> rebuildZoneConnections() const;
	bool                              checkConnections(const QMap<QPair<int, int>, int> &connections, const QMap<int, QByteArray> &names);

	static QString toString(QList<int> const &map, int width);

	QFileInfo          m_file;
	Properties         m_properties;
	Water              m_water;
	Data               m_data;
	QList<Layer>       m_layers;
	QList<Environment> m_environments;
	QList<Track>       m_tracks;
	QList<Object>      m_objects;
	bool               m_preview{false};
	QString            m_ai;
	QString            m_error;
};

}

Q_DECLARE_METATYPE(Alamo::Map::Layer)
Q_DECLARE_METATYPE(Alamo::Map::Environment)
Q_DECLARE_METATYPE(Alamo::Map::Data)

#endif
