//------------------------------------------------------------------------------
//
// Original source code: https://github.com/GlyphXTools/alo-viewer
//
//------------------------------------------------------------------------------

#include "Models.hpp"

namespace Alamo
{

Model::Model(QFileInfo const &file)
{
	m_name = file.completeBaseName();
	try
	{
		ChunkFile reader{file};

		readSkeleton(reader);
		while (reader.next() && (reader.type() == ChunkMesh || reader.type() == ChunkLight))
		{
			if (reader.type() == ChunkMesh)
			{
				m_meshes << Mesh{};
				m_meshes.last().index = m_meshes.count() - 1;
				readMesh(reader, m_meshes.last());
			}
			else
			{
				m_lights << Light{};
				readLight(reader, m_lights.last());
			}
		}
		ChunkFile::verify(reader.type() == ChunkConnection);
		readConnections(reader);
	}
	catch (std::exception const &exception)
	{
		m_error = exception.what();
	}
	if (!m_error.isEmpty())
		qWarning() << __FUNCTION__ << __LINE__ << m_error;
}

void Model::recalculateAbsoluteTransforms()
{
	auto indexes{computeBoneIndexes()};

	for (qsizetype index{0}; index < indexes.count(); ++index)
	{
		auto &bone{m_bones[indexes[index]]};

		bone.absoluteTransform = (bone.parent > -1 ? m_bones[bone.parent].absoluteTransform * bone.relativeTransform : bone.relativeTransform);
		bone.invertedAbsoluteTransform = bone.absoluteTransform.inverted();
	}
}

void Model::recalculateRelativeTransforms()
{
	auto indexes{computeBoneIndexes()};

	for (qsizetype index{indexes.count() - 1}; index >= 0; --index)
	{
		auto &bone{m_bones[indexes[index]]};

		bone.invertedAbsoluteTransform = bone.absoluteTransform.inverted();
		bone.relativeTransform = (bone.parent > -1 ? m_bones[bone.parent].absoluteTransform.inverted() * bone.absoluteTransform : bone.absoluteTransform);
	}
}

QList<int> Model::computeBoneIndexes() const
{
	QList<QPair<int, int>> parentChidlList;
	QList<int>             indexes;

	for (int index{0}; index < m_bones.count(); ++index)
		if (!m_deletedBones.contains(index))
			parentChidlList.append({m_bones[index].parent, index});
	while (parentChidlList.count())
	{
		auto iterator{parentChidlList.begin()};

		while (iterator->first != -1 && !indexes.contains(iterator->first))
			if (++iterator == parentChidlList.end())
				return indexes;
		indexes << iterator->second;
		parentChidlList.removeAll(*iterator);
	}
	return indexes;
}

QList<int> Model::computeObjectIndexes() const
{
	QList<int> indexes;

	for (auto const &mesh: m_meshes)
		if (!m_deletedMeshes.contains(mesh.index))
			indexes << mesh.index;
	for (int index{0}; index < m_lights.count(); ++index)
		indexes << m_meshes.count() + index;
	return indexes;
}

void Model::save(QFileInfo const &file, double scale)
{
	try
	{
		ChunkFile writer{file, QFile::WriteOnly};

		writeSkeleton(writer, scale);
		for (auto &mesh: m_meshes)
			if (!m_deletedMeshes.contains(mesh.index))
			{
				writer.beginChunk(ChunkMesh);
				writeMesh(writer, mesh, scale);
				writer.endChunk();
			}
		for (auto &light: m_lights)
		{
			writer.beginChunk(ChunkLight);
			writeLight(writer, light);
			writer.endChunk();
		}
		writer.beginChunk(ChunkConnection);
		writeConnections(writer);
		writer.endChunk();
	}
	catch (const std::exception &exception)
	{
		m_error = exception.what();
	}
	if (!m_error.isEmpty())
		qWarning() << __FUNCTION__ << __LINE__ << m_error;
}

void Model::readBone(ChunkFile &reader, Bone &bone)
{
	ChunkFile::verify(reader.next() == ChunkSkeletonBoneName);
	bone.name = reader.readString();
	ChunkType type{reader.next()};
	ChunkFile::verify(type == ChunkSkeletonBoneBase || type == ChunkSkeletonBoneAdvanced);
	bone.parent = static_cast<long>(reader.readInteger());
	bone.visible = reader.readInteger();
	bone.billboard = Disable;
	if (type == ChunkSkeletonBoneAdvanced)
		bone.billboard = static_cast<Billboard>(reader.readInteger());
	bone.relativeTransform = QMatrix4x4{};
	for (int index{0}; index < 12; ++index)
		bone.relativeTransform.data()[index] = reader.readFloat();
	bone.relativeTransform = bone.relativeTransform.transposed();
	bone.absoluteTransform = (bone.parent > -1 ? m_bones[bone.parent].absoluteTransform * bone.relativeTransform : bone.relativeTransform);
	bone.invertedAbsoluteTransform = bone.absoluteTransform.inverted();
	ChunkFile::verify(reader.next() == ChunkEnd);
}

void Model::readSkeleton(ChunkFile &reader)
{
	ChunkFile::verify(reader.next() == ChunkSkeleton);
	ChunkFile::verify(reader.next() == ChunkSkeletonSize);
	m_bones.resize(reader.readInteger());
	for (int index{0}; index < m_bones.size(); ++index)
	{
		ChunkFile::verify(reader.next() == ChunkSkeletonBone);
		readBone(reader, m_bones[index]);
		m_bones[index].index = index;
	}
	ChunkFile::verify(reader.next() == ChunkEnd);
}

void Model::readSubMesh(ChunkFile &reader, SubMesh &subMesh)
{
	ChunkType type;

	ChunkFile::verify(reader.next() == ChunkShader);
	ChunkFile::verify(reader.next() == ChunkShaderName);
	subMesh.shader = reader.readString();
	type = reader.next();
	while (type != -1) // Read shader parameters
	{
		ShaderParameter parameter;

		ChunkFile::verify(reader.nextMini() == 1);
		parameter.name = reader.readString();
		parameter.colorize = (m_meshes[subMesh.mesh].name.contains("FC_") == 0 && parameter.name.compare("Color", Qt::CaseInsensitive) == 0);
		ChunkFile::verify(reader.nextMini() == 2);
		if (type == ChunkShaderParameterInt)
		{
			parameter.type = ShaderParameter::Int;
			parameter.integer = reader.readInteger();
		}
		else if (type == ChunkShaderParameterFloat)
		{
			parameter.type = ShaderParameter::Float;
			parameter.vector1 = reader.readFloat();
		}
		else if (type == ChunkShaderParameterFloat3)
		{
			parameter.type = ShaderParameter::Float3;
			parameter.vector3 = reader.readVector3();
		}
		else if (type == ChunkShaderParameterTexture)
		{
			parameter.type = ShaderParameter::Texture;
			parameter.texture = reader.readString();
		}
		else if (type == ChunkShaderParameterFloat4)
		{
			parameter.type = ShaderParameter::Float4;
			parameter.vector4 = reader.readVector4();
		}
		subMesh.parameters << parameter;
		ChunkFile::verify(reader.nextMini() == -1);
		type = reader.next();
	}
	ChunkFile::verify(reader.next() == ChunkSubMesh);
	ChunkFile::verify(reader.next() == ChunkSubMeshSize);
	subMesh.vertices.resize(reader.readInteger());
	subMesh.indices.resize(reader.readInteger() * 3);
	ChunkFile::verify(reader.next() == ChunkSubMeshVertexFormat);
	subMesh.vertexFormat = reader.readString();
	type = reader.next();
	ChunkFile::verify(type == ChunkSubMeshVertexData);
	reader.read(subMesh.vertices.data(), subMesh.vertices.size() * sizeof(MASTER_VERTEX));
	ChunkFile::verify(reader.next() == ChunkSubMeshIndexData);
	reader.read(subMesh.indices.data(), subMesh.indices.size() * sizeof(uint16_t));
	type = reader.next();
	subMesh.nSkinBones = 0;
	if (type == ChunkSubMeshSkinData)
	{
		while (reader.tell() != static_cast<qint64>(reader.size()) && subMesh.nSkinBones < MAX_NUM_SKIN_BONES)
			subMesh.skin[subMesh.nSkinBones++] = reader.readInteger();
		type = reader.next();
	}
	if (type == ChunkSubMeshCollisionTree)
	{
		ChunkFile::verify(reader.next() == ChunkSubMeshCollisionTree0);
		subMesh.collisionTree[0].resize(reader.size());
		reader.read(subMesh.collisionTree[0].data(), reader.size());
		ChunkFile::verify(reader.next() == ChunkSubMeshCollisionTree1);
		subMesh.collisionTree[1].resize(reader.size());
		reader.read(subMesh.collisionTree[1].data(), reader.size());
		ChunkFile::verify(reader.next() == ChunkSubMeshCollisionTree2);
		subMesh.collisionTree[2].resize(reader.size());
		reader.read(subMesh.collisionTree[2].data(), reader.size());
		ChunkFile::verify(reader.next() == ChunkEnd);
		type = reader.next();
	}
	ChunkFile::verify(type == -1);
}

void Model::readMesh(ChunkFile &reader, Mesh &mesh)
{
	ChunkFile::verify(reader.next() == ChunkMeshName);
	mesh.name = reader.readString();
	ChunkFile::verify(reader.next() == ChunkMeshInformation);
	mesh.subMeshes.resize( reader.readInteger());
	mesh.bounds.min = reader.readVector3();
	mesh.bounds.max = reader.readVector3();
	reader.readInteger();
	mesh.visible = (reader.readInteger() == 0);
	mesh.collidable = (reader.readInteger() != 0);
	for (int index{0}; index < mesh.subMeshes.size(); ++index)
	{
		mesh.subMeshes[index].index = index;
		mesh.subMeshes[index].mesh = mesh.index;
		readSubMesh(reader, mesh.subMeshes[index]);
	}
	ChunkFile::verify(reader.next() == ChunkEnd);
}

void Model::readLight(ChunkFile &reader, Light &light)
{
	ChunkFile::verify(reader.next() == ChunkLightName);
	light.name = reader.readString();
	ChunkFile::verify(reader.next() == ChunkLightInformation);
	light.type = static_cast<LightType>(reader.readInteger());
	light.color = reader.readColorRGB();
	light.intensity = reader.readFloat();
	light.farAttenuationEnd = reader.readFloat();
	light.farAttenuationStart = reader.readFloat();
	light.hotspotSize = reader.readFloat();
	light.falloffSize = reader.readFloat();
	ChunkFile::verify(reader.next() == ChunkEnd);
}

void Model::readConnections(ChunkFile &reader)
{
	unsigned long connectionCount{0};
	unsigned long proxyCount{0};

	ChunkFile::verify(reader.next() == ChunkConnectionSize);
	ChunkFile::verify(reader.nextMini() == 1);
	connectionCount = reader.readInteger();
	ChunkFile::verify(reader.nextMini() == 4);
	proxyCount = reader.readInteger();
	while (reader.nextMini() != -1)
		qDebug() << __FUNCTION__ << __LINE__ << reader.type();
	ChunkFile::verify(reader.type() == ChunkEnd);
	m_connections.resize(connectionCount);
	for (auto &connection: m_connections)
	{
		ChunkFile::verify(reader.next() == ChunkConnectionInformation);
		ChunkFile::verify(reader.nextMini() == 2);
		connection.object = reader.readInteger();
		ChunkFile::verify(reader.nextMini() == 3);
		connection.bone = reader.readInteger();
		ChunkFile::verify(reader.nextMini() == -1);
		if (connection.object < m_meshes.count())
			m_meshes[connection.object].bone = connection.bone; // Connect the object to the bone
		else
			m_lights[connection.object - m_meshes.count()].bone = connection.bone; // Connect the object to the bone
	}
	m_proxies.resize(proxyCount);
	for (auto &proxy: m_proxies)
	{
		proxy.visible = true;
		proxy.altDecreaseStayHidden = false;
		ChunkFile::verify(reader.next() == ChunkConnectionProxy);
		ChunkFile::verify(reader.nextMini() == 5);
		proxy.name = reader.readString();
		ChunkFile::verify(reader.nextMini() == 6);
		proxy.bone = reader.readInteger();
		reader.nextMini();
		if (reader.type() == 7)
		{
			proxy.visible = (reader.readInteger() == 0);
			reader.nextMini();
		}
		if (reader.type() == 8)
		{
			proxy.altDecreaseStayHidden = (reader.readInteger() != 0);
			reader.nextMini();
		}
		ChunkFile::verify(reader.type() == ChunkEnd);
	}
	ChunkFile::verify(reader.next() == ChunkEnd);
}

void Model::writeBone(ChunkFile &writer, Bone const &bone) const
{
	QMatrix4x4 relativeTransform;

	writer.beginChunk(ChunkSkeletonBoneName);
	writer.writeString(bone.name);
	writer.endChunk();
	writer.beginChunk(ChunkSkeletonBoneAdvanced);
	writer.writeInteger(bone.parent);
	writer.writeInteger(bone.visible);
	writer.writeInteger(bone.billboard);
	relativeTransform = bone.relativeTransform.transposed();
	for (int index{0}; index < 12; ++index)
		writer.writeFloat(relativeTransform.data()[index]);
	writer.endChunk();
}

void Model::writeSkeleton(ChunkFile &writer, double scale) const
{
	QList<int> indexes{computeBoneIndexes()};

	writer.beginChunk(ChunkSkeleton);
	writer.beginChunk(ChunkSkeletonSize);
	writer.writeInteger(m_bones.count() - m_deletedBones.count());
	for (int index{0}; index < 31; ++index)
		writer.writeInteger(0);
	writer.endChunk();
	for (int index: indexes)
	{
		Bone bone{m_bones[index]};

		bone.parent = indexes.indexOf(bone.parent);
		if (scale != 1)
			bone.relativeTransform.setColumn(3, bone.relativeTransform.column(3) * scale);
		writer.beginChunk(ChunkSkeletonBone);
		writeBone(writer, bone);
		writer.endChunk();
	}
	writer.endChunk();
}

void Model::writeSubMesh(ChunkFile &writer, SubMesh const &subMesh) const
{
	writer.beginChunk(ChunkShader);
	writer.beginChunk(ChunkShaderName);
	writer.writeString(subMesh.shader);
	writer.endChunk();
	for (auto const &parameter: subMesh.parameters)
	{
		if (parameter.type == ShaderParameter::Int)
			writer.beginChunk(ChunkShaderParameterInt);
		if (parameter.type == ShaderParameter::Float)
			writer.beginChunk(ChunkShaderParameterFloat);
		if (parameter.type == ShaderParameter::Float3)
			writer.beginChunk(ChunkShaderParameterFloat3);
		if (parameter.type == ShaderParameter::Texture)
			writer.beginChunk(ChunkShaderParameterTexture);
		if (parameter.type == ShaderParameter::Float4)
			writer.beginChunk(ChunkShaderParameterFloat4);
		writer.beginMiniChunk(1);
		writer.writeString(parameter.name);
		writer.endChunk();
		writer.beginMiniChunk(2);
		if (parameter.type == ShaderParameter::Int)
			writer.writeInteger(parameter.integer);
		if (parameter.type == ShaderParameter::Float)
			writer.writeFloat(parameter.vector1);
		if (parameter.type == ShaderParameter::Float3)
			writer.writeVector3(parameter.vector3);
		if (parameter.type == ShaderParameter::Texture)
			writer.writeString(parameter.texture);
		if (parameter.type == ShaderParameter::Float4)
			writer.writeVector4(parameter.vector4);
		writer.endChunk();
		writer.endChunk();
	}
	writer.endChunk();
	writer.beginChunk(ChunkSubMesh);
	writer.beginChunk(ChunkSubMeshSize);
	writer.writeInteger(subMesh.vertices.size());
	writer.writeInteger(subMesh.indices.size() / 3);
	for (int index{0}; index < 30; ++index)
		writer.writeInteger(0);
	writer.endChunk();
	writer.beginChunk(ChunkSubMeshVertexFormat);
	writer.writeString(subMesh.vertexFormat);
	writer.endChunk();
	writer.beginChunk(ChunkSubMeshVertexData);
	writer.write(subMesh.vertices.data(), subMesh.vertices.count() * sizeof(MASTER_VERTEX));
	writer.endChunk();
	writer.beginChunk(ChunkSubMeshIndexData);
	writer.write(subMesh.indices.data(), subMesh.indices.count() * sizeof(uint16_t));
	writer.endChunk();
	if (subMesh.nSkinBones)
	{
		writer.beginChunk(ChunkSubMeshSkinData);
		for (qsizetype index = 0; index < subMesh.nSkinBones; ++index)
			writer.writeInteger(subMesh.skin[index]);
		writer.endChunk();
	}
	if (!subMesh.collisionTree[0].isEmpty())
	{
		writer.beginChunk(ChunkSubMeshCollisionTree);
		writer.beginChunk(ChunkSubMeshCollisionTree0);
		writer.write(subMesh.collisionTree[0].data(), subMesh.collisionTree[0].size());
		writer.endChunk();
		writer.beginChunk(ChunkSubMeshCollisionTree1);
		writer.write(subMesh.collisionTree[1].data(), subMesh.collisionTree[1].size());
		writer.endChunk();
		writer.beginChunk(ChunkSubMeshCollisionTree2);
		writer.write(subMesh.collisionTree[2].data(), subMesh.collisionTree[2].size());
		writer.endChunk();
		writer.endChunk();
	}
	writer.endChunk();
}

void Model::writeMesh(ChunkFile &writer, Mesh const &mesh, double scale) const
{
	writer.beginChunk(ChunkMeshName);
	writer.writeString(mesh.name);
	writer.endChunk();
	writer.beginChunk(ChunkMeshInformation);
	writer.writeInteger(mesh.subMeshes.count());
	writer.writeVector3(mesh.bounds.min);
	writer.writeVector3(mesh.bounds.max);
	writer.writeInteger(0);
	writer.writeInteger(mesh.visible == 0);
	writer.writeInteger(mesh.collidable);
	for (int index = 0; index < 22; ++index)
		writer.writeInteger(0);
	writer.endChunk();
	for (auto const &subMesh: mesh.subMeshes)
	{
		if (scale != 1)
		{
			auto scaled{subMesh};

			for (auto &vertex: scaled.vertices)
				vertex.position *= scale;
			writeSubMesh(writer, scaled);
		}
		else
			writeSubMesh(writer, subMesh);
	}
}

void Model::writeLight(ChunkFile &writer, Light const &light) const
{
	writer.beginChunk(ChunkLightName);
	writer.writeString(light.name);
	writer.endChunk();
	writer.beginChunk(ChunkLightInformation);
	writer.writeInteger(light.type);
	writer.writeColorRGB(light.color);
	writer.writeFloat(light.intensity);
	writer.writeFloat(light.farAttenuationEnd);
	writer.writeFloat(light.farAttenuationStart);
	writer.writeFloat(light.hotspotSize);
	writer.writeFloat(light.falloffSize);
	writer.endChunk();
}

void Model::writeConnections(ChunkFile &writer) const
{
	QList<int>        boneIndexes{computeBoneIndexes()};
	QList<int>        objectIndexes{computeObjectIndexes()};
	QList<Connection> connections;
	QList<Proxy>      proxies;

	for (auto const &connection: m_connections)
		if (!m_deletedBones.contains(connection.bone) && !m_deletedMeshes.contains(connection.object))
			connections << connection;
	for (int index{0}; index < m_proxies.count(); ++index)
		if (!m_deletedProxies.contains(index) && !m_deletedBones.contains(m_proxies[index].bone))
			proxies << m_proxies[index];
	writer.beginChunk(ChunkConnectionSize);
	writer.beginMiniChunk(1);
	writer.writeInteger(connections.count());
	writer.endChunk();
	writer.beginMiniChunk(4);
	writer.writeInteger(proxies.count());
	writer.endChunk();
	writer.endChunk();
	for (auto const &connection: connections)
	{
		writer.beginChunk(ChunkConnectionInformation);
		writer.beginMiniChunk(2);
		writer.writeInteger(objectIndexes.indexOf(connection.object));
		writer.endChunk();
		writer.beginMiniChunk(3);
		writer.writeInteger(boneIndexes.indexOf(connection.bone));
		writer.endChunk();
		writer.endChunk();
	}
	for (auto const &proxy: proxies)
	{
		writer.beginChunk(ChunkConnectionProxy);
		writer.beginMiniChunk(5);
		writer.writeString(proxy.name);
		writer.endChunk();
		writer.beginMiniChunk(6);
		writer.writeInteger(boneIndexes.indexOf(proxy.bone));
		writer.endChunk();
		writer.beginMiniChunk(7);
		writer.writeInteger(proxy.visible == 0);
		writer.endChunk();
		writer.beginMiniChunk(8);
		writer.writeInteger(proxy.altDecreaseStayHidden != 0);
		writer.endChunk();
		writer.endChunk();
	}
}

}
