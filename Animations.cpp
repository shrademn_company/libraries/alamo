//------------------------------------------------------------------------------
//
// Original source code: https://github.com/GlyphXTools/alo-viewer
//
//------------------------------------------------------------------------------

#include <QDebug>
#include <QSet>
#include <QtMath>

#include "Animations.hpp"
#include "ChunkFile.hpp"

using namespace std;

namespace Alamo
{

Animation::Animation(QFileInfo const &file, Model const &model)
{
	m_name = file.completeBaseName();
	m_suffix = m_name.sliced(model.name().length());
	try
	{
		ChunkFile reader{file};

		readAnimation(reader, model);
	}
	catch (std::exception const &exception)
	{
		m_error = exception.what();
	}
	if (!m_error.isEmpty())
		qWarning() << __FUNCTION__ << __LINE__ << m_error;
}

Animation::Frame const &Animation::getFrameInfo(size_t bone, size_t frame) const
{
	return m_frames[bone * (m_frameCount + 1) + frame];
}

bool Animation::isVisible(size_t bone, float time) const
{
	size_t frame{m_frameCount > 0 ? static_cast<size_t>(time * m_fps) % m_frameCount : 0};
	size_t base{bone *(m_frameCount + 1)};

	return m_frames[base + frame].visible;
}

float Animation::getVisibleEvent(size_t bone, float from, float to) const
{
	assert(from <= to);
	size_t f1 = qCeil(from * m_fps);
	size_t base = bone * (m_frameCount + 1);
	if (m_frameCount != 0)
	{
		size_t n = qMax(qFloor(to * m_fps), static_cast<int>(f1)) - f1;
		for (size_t f = 0; f <= n; f++)
		{
			size_t i = (f1 + f);
			if (m_frames[base + (i % m_frameCount)].visible)
				return i / m_fps;
		}
	}
	else if (m_frames[base].visible)
		return 0.0f;
	return -1;
}

float Animation::getInvisibleEvent(size_t bone, float from, float to) const
{
	assert(from <= to);
	size_t f1 = qCeil(from * m_fps);
	size_t base = bone * (m_frameCount + 1);
	if (m_frameCount != 0)
	{
		size_t n = qMax(qFloor(to * m_fps), static_cast<int>(f1)) - f1;
		for (size_t f = 0; f <= n; f++)
		{
			size_t i = (f1 + f);
			if (!m_frames[base + (i % m_frameCount)].visible)
				return i / m_fps;
		}
	}
	else if (!m_frames[base].visible)
		return 0.0f;
	return -1;
}

int Animation::getVisibleFrame(size_t bone, size_t from, size_t to) const
{
	size_t base{(m_frameCount + 1) * bone};

	if (m_frameCount)
	{
		for (size_t frame{from}; frame <= to; ++frame)
			if (m_frames[base + frame % m_frameCount].visible)
				return frame;
	}
	else if (m_frames[base].visible)
		return 0;
	return -1;
}

int Animation::getInvisibleFrame(size_t bone, size_t from, size_t to) const
{
	size_t base{(m_frameCount + 1) * bone};

	if (m_frameCount)
	{
		for (size_t frame = from; frame <= to; ++frame)
			if (!m_frames[base + frame % m_frameCount].visible)
				return frame;
	}
	else if (!m_frames[base].visible)
		return 0;
	return -1;
}

void Animation::save(QFileInfo const &file, Model const &model)
{
	try
	{
		ChunkFile writer{file, QFile::WriteOnly};

		++m_frameCount;
		writeAnimation(writer, model);
		--m_frameCount;
	}
	catch (const std::exception &exception)
	{
		m_error = exception.what();
	}
	if (!m_error.isEmpty())
		qWarning() << __FUNCTION__ << __LINE__ << m_error;
}

void Animation::readAnimation(ChunkFile &reader, Model const &model)
{
	ChunkFile::verify(reader.next() == ChunkAnimation);
	ChunkFile::verify(reader.next() == ChunkAnimationInformation);
	ChunkFile::verify(reader.nextMini() == 1);
	m_frameCount = reader.readInteger();
	ChunkFile::verify(reader.nextMini() == 2);
	m_fps = reader.readFloat();
	ChunkFile::verify(reader.nextMini() == 3);
	m_bones.resize(reader.readInteger());
	ChunkFile::verify(reader.nextMini() == 11);
	m_data.rotationBlockSize = reader.readInteger() / (sizeof(PackedQuaternion) / sizeof(int16_t));
	ChunkFile::verify(reader.nextMini() == 12);
	m_data.translationBlockSize = reader.readInteger() / (sizeof(PackedVector) / sizeof(uint16_t));
	ChunkFile::verify(reader.nextMini() == 13);
	m_data.scaleBlockSize = reader.readInteger() / (sizeof(PackedVector) / sizeof(uint16_t));
	reader.nextMini();
	ChunkFile::verify(reader.type() == ChunkEnd);
	m_data.rotationData.resize(m_data.rotationBlockSize * m_frameCount);
	m_data.translationData.resize(m_data.translationBlockSize * m_frameCount);
	m_data.scaleData.resize(m_data.scaleBlockSize * m_frameCount);
	m_frames.resize(model.bones().count() * m_frameCount);
	for (int index = 0; index < m_bones.size(); ++index)
		readBone(reader, model, m_bones[index]);
	if (m_data.translationBlockSize > 0)
	{
		ChunkFile::verify(reader.next() == ChunkAnimationTranslationData);
		reader.read(m_data.translationData.data(), m_data.translationData.size() * sizeof(PackedVector));
	}
	if (m_data.rotationBlockSize > 0)
	{
		ChunkFile::verify(reader.next() == ChunkAnimationRotationData);
		reader.read(m_data.rotationData.data(), m_data.rotationData.size() * sizeof(PackedQuaternion));
	}
	ChunkFile::verify(reader.next() == ChunkEnd);
	constructTransforms(model);
	--m_frameCount; // There are actually one less frames in the animation, the first is duplicated at the end for easy looping.
}

void Animation::readBone(ChunkFile &reader, Model const &model, Bone &bone)
{
	QByteArray name;

	ChunkFile::verify(reader.next() == ChunkBone);
	ChunkFile::verify(reader.next() == ChunkBoneInformation);
	ChunkFile::verify(reader.nextMini() == 4);
	name = reader.readString();
	ChunkFile::verify(reader.nextMini() == 5);
	bone.index = reader.readInteger();
	ChunkFile::verify(bone.index < model.bones().count());
	reader.nextMini();
	if (reader.type() == 10)
		reader.nextMini(); // We ignore this anyway
	ChunkFile::verify(reader.type() == 6);
	bone.ofsTranslation = reader.readVector3();
	ChunkFile::verify(reader.nextMini() == 7);
	bone.scaleTranslatioon = reader.readVector3();
	ChunkFile::verify(reader.nextMini() == 8);
	bone.ofsScale = reader.readVector3();
	ChunkFile::verify(reader.nextMini() == 9);
	bone.scaleScale = reader.readVector3();
	ChunkFile::verify(reader.nextMini() == 14);
	bone.indexTranslation = reader.readShort();
	if (bone.indexTranslation != UINT16_MAX)
		bone.indexTranslation /= (sizeof(PackedVector) / sizeof(uint16_t));
	ChunkFile::verify(reader.nextMini() == 15);
	bone.indexScale = reader.readShort();
	if (bone.indexScale != UINT16_MAX)
		bone.indexScale /= (sizeof(PackedVector) / sizeof(uint16_t));
	ChunkFile::verify(reader.nextMini() == 16);
	bone.indexRotation = reader.readShort();
	if (bone.indexRotation != UINT16_MAX)
		bone.indexRotation /= (sizeof(PackedQuaternion) / sizeof(int16_t));
	ChunkFile::verify(reader.nextMini() == 17);
	bone.defRotation = PackedQuaternion{reader}.unpack();
	ChunkFile::verify(reader.nextMini() == ChunkEnd);
	reader.next();
	if (reader.type() == ChunkBoneVisibilityData)
	{
		bone.visibilityData.resize((m_frameCount + 7) / 8);
		reader.read(bone.visibilityData.data(), bone.visibilityData.size());
		for (size_t frame{0}; frame < m_frameCount; ++frame)
			m_frames[bone.index * m_frameCount + frame].visible = ((bone.visibilityData[frame / 8] >> (frame % 8)) != 0);
		reader.next();
	}
	if (reader.type() == 0x1008)
		reader.next(); // Skip this track
	ChunkFile::verify(reader.type() == ChunkEnd);
}

void Animation::constructTransforms(Model const &model)
{
	QList<QMatrix4x4> transforms;
	QSet<size_t>      animated;

	transforms.resize(model.bones().count() * m_frameCount);
	for (auto const &bone: m_bones) // Keep track of which bones are animated
	{
		for (size_t frame{0}; frame < m_frameCount; ++frame)
		{
			QVector3D   translation{bone.ofsTranslation};
			QMatrix4x4  translationMatrix;
			QVector3D   scale{bone.ofsScale};
			QQuaternion rotation{bone.defRotation};
			QMatrix4x4  rotationMatrix;

			if (bone.indexTranslation != UINT16_MAX)
				translation += m_data.translationData[frame * m_data.translationBlockSize + bone.indexTranslation].unpack() * bone.scaleTranslatioon;
			if (bone.indexScale != UINT16_MAX)
				scale += m_data.scaleData[frame * m_data.scaleBlockSize + bone.indexScale].unpack() * bone.scaleScale;
			if (bone.indexRotation != UINT16_MAX)
				rotation = m_data.rotationData[frame * m_data.rotationBlockSize + bone.indexRotation].unpack();
			translationMatrix.translate(translation * scale);
			rotationMatrix.rotate(rotation);
			if (model.bones()[bone.index].parent > -1)
				transforms[bone.index * m_frameCount + frame] = transforms[model.bones()[bone.index].parent * m_frameCount + frame] * translationMatrix * rotationMatrix;
			else
				transforms[bone.index * m_frameCount + frame] = translationMatrix * rotationMatrix;
		}
		animated.insert(bone.index);
	}
	for (qsizetype index{0}; index < model.bones().count(); ++index)
	{
		if (!animated.contains(index))
			for (size_t frame{0}; frame < m_frameCount; ++frame) // Fill unanimated bones with default relative transforms
				transforms[index * m_frameCount + frame] = model.bones()[index].relativeTransform;
		for (size_t frame{0}; frame < m_frameCount; ++frame) // Decompose animations into SRT components
		{
			Frame      &frameInfo{m_frames[index * m_frameCount + frame]};
			QMatrix3x3 matrix;

			frameInfo.translation = transforms[index * m_frameCount + frame].column(3).toVector3D();
			frameInfo.scale = {transforms[index * m_frameCount + frame].column(0).length(),
							   transforms[index * m_frameCount + frame].column(1).length(),
							   transforms[index * m_frameCount + frame].column(2).length()};
			matrix.data()[0] = transforms[index * m_frameCount + frame].column(0).x() / frameInfo.scale.x();
			matrix.data()[1] = transforms[index * m_frameCount + frame].column(0).y() / frameInfo.scale.x();
			matrix.data()[2] = transforms[index * m_frameCount + frame].column(0).z() / frameInfo.scale.x();
			matrix.data()[3] = transforms[index * m_frameCount + frame].column(1).x() / frameInfo.scale.y();
			matrix.data()[4] = transforms[index * m_frameCount + frame].column(1).y() / frameInfo.scale.y();
			matrix.data()[5] = transforms[index * m_frameCount + frame].column(1).z() / frameInfo.scale.y();
			matrix.data()[6] = transforms[index * m_frameCount + frame].column(2).x() / frameInfo.scale.z();
			matrix.data()[7] = transforms[index * m_frameCount + frame].column(2).y() / frameInfo.scale.z();
			matrix.data()[8] = transforms[index * m_frameCount + frame].column(2).z() / frameInfo.scale.z();
			frameInfo.rotation = QQuaternion::fromRotationMatrix(matrix);
		}
	}
}

void Animation::writeAnimation(ChunkFile &writer, Model const &model)
{
	QList<int> boneIndexes{model.computeBoneIndexes()};
	auto const bones{computeBones(boneIndexes)};

	writer.beginChunk(ChunkAnimation);
	writer.beginChunk(ChunkAnimationInformation);
	writer.beginMiniChunk(1);
	writer.writeInteger(m_frameCount);
	writer.endChunk();
	writer.beginMiniChunk(2);
	writer.writeFloat(m_fps);
	writer.endChunk();
	writer.beginMiniChunk(3);
	writer.writeInteger(bones.count());
	writer.endChunk();
	writer.beginMiniChunk(11);
	writer.writeInteger(m_data.rotationBlockSize * (sizeof(PackedQuaternion) / sizeof(int16_t)));
	writer.endChunk();
	writer.beginMiniChunk(12);
	writer.writeInteger(m_data.translationBlockSize * (sizeof(PackedVector) / sizeof(uint16_t)));
	writer.endChunk();
	writer.beginMiniChunk(13);
	writer.writeInteger(m_data.scaleBlockSize * (sizeof(PackedVector) / sizeof(uint16_t)));
	writer.endChunk();
	writer.endChunk();
	for (auto const &bone: bones)
		writeBone(writer, model.bones()[boneIndexes[bone.index]].name, bone);
	if (m_data.translationBlockSize > 0)
	{
		writer.beginChunk(ChunkAnimationTranslationData);
		writer.write(m_data.translationData.data(), m_data.translationData.size() * sizeof(PackedVector));
		writer.endChunk();
	}
	if (m_data.rotationBlockSize > 0)
	{
		writer.beginChunk(ChunkAnimationRotationData);
		writer.write(m_data.rotationData.data(), m_data.rotationData.size() * sizeof(PackedQuaternion));
		writer.endChunk();
	}
	writer.endChunk();
}

void Animation::writeBone(ChunkFile &writer, QByteArray const &name, Bone const &bone)
{
	writer.beginChunk(ChunkBone);
	writer.beginChunk(ChunkBoneInformation);
	writer.beginMiniChunk(4);
	writer.writeString(name);
	writer.endChunk();
	writer.beginMiniChunk(5);
	writer.writeInteger(bone.index);
	writer.endChunk();
	writer.beginMiniChunk(6);
	writer.writeVector3(bone.ofsTranslation);
	writer.endChunk();
	writer.beginMiniChunk(7);
	writer.writeVector3(bone.scaleTranslatioon);
	writer.endChunk();
	writer.beginMiniChunk(8);
	writer.writeVector3(bone.ofsScale);
	writer.endChunk();
	writer.beginMiniChunk(9);
	writer.writeVector3(bone.scaleScale);
	writer.endChunk();
	writer.beginMiniChunk(14);
	if (bone.indexTranslation != UINT16_MAX)
		writer.writeShort(bone.indexTranslation * (sizeof(PackedVector) / sizeof(uint16_t)));
	else
		writer.writeShort(bone.indexTranslation);
	writer.endChunk();
	writer.beginMiniChunk(15);
	if (bone.indexScale != UINT16_MAX)
		writer.writeShort(bone.indexScale * (sizeof(PackedVector) / sizeof(uint16_t)));
	else
		writer.writeShort(bone.indexScale);
	writer.endChunk();
	writer.beginMiniChunk(16);
	if (bone.indexRotation != UINT16_MAX)
		writer.writeShort(bone.indexRotation * (sizeof(PackedQuaternion) / sizeof(int16_t)));
	else
		writer.writeShort(bone.indexRotation);
	writer.endChunk();
	writer.beginMiniChunk(17);
	writer.writeShort(PackedQuaternion{bone.defRotation}.x);
	writer.writeShort(PackedQuaternion{bone.defRotation}.y);
	writer.writeShort(PackedQuaternion{bone.defRotation}.z);
	writer.writeShort(PackedQuaternion{bone.defRotation}.w);
	writer.endChunk();
	writer.endChunk();
	if (!bone.visibilityData.isEmpty())
	{
		writer.beginChunk(ChunkBoneVisibilityData);
		writer.write(bone.visibilityData.data(), bone.visibilityData.size());
		writer.endChunk();
	}
	writer.endChunk();
}

QList<Animation::Bone> Animation::computeBones(QList<int> const &boneIndexes) const
{
	QList<Bone>    bones;
	QMap<int, int> indexMapping;

	for (int index{0}; index < m_bones.count(); ++index)
		indexMapping[m_bones[index].index] = index;
	for (int index: boneIndexes)
		if (index)
		{
			auto bone{m_bones.value(indexMapping.value(index, -1))};

			bone.index = boneIndexes.indexOf(index);
			bones << bone;
		}
	return bones;
}

}
