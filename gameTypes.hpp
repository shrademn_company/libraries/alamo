//------------------------------------------------------------------------------
//
// Original source code: https://github.com/GlyphXTools/alo-viewer
//
//------------------------------------------------------------------------------

#ifndef GAMETYPES_H
#define GAMETYPES_H

#include <QByteArray>
#include <QColor>
#include <QVector4D>

namespace Alamo
{

enum ShaderDetail
{
	FixedFunction     = 0,
	DX8               = 1,
	DX8ATI            = 2,
	DX9               = 3,
	ShaderDetailCount = 4
};

struct RenderSettings
{
	unsigned long screenWidth;
	unsigned long screenHeight;
	unsigned long screenRefresh;
	bool          softShadows;
	bool          antiAlias;
	ShaderDetail  shaderDetail;
	bool          bloom;
	bool          heatDistortion;
	bool          heatDebug;
	bool          shadowDebug;
};

struct Range
{
	float min;
	float max;
};

struct DirectionalLight
{
	QColor    color;
	QVector3D direction;
};

struct Camera
{
	QVector3D position;
	QVector3D target;
	QVector3D up;
};

struct Wind
{
	float speed;
	float heading;
};

struct Environment
{
	DirectionalLight lights[3];
	QColor           specular;
	QColor           ambient;
	QColor           shadow;
	Wind             wind;
	QColor           clearColor;
	QVector3D        gravity;
};

struct BoundingBox
{
	QVector3D min;
	QVector3D max;
};

enum Billboard
{
	Disable,
	Parallel,
	Face,
	ZAxisView,
	ZAxisLight,
	ZaxisWind,
	SunlightGlow,
	Sun
};

struct ShaderParameter
{
	enum Type
	{
		Int,
		Float,
		Float3,
		Float4,
		Texture
	};

	Type       type;
	QByteArray name;
	bool       colorize{false};
	int        integer{0};
	float      vector1{0};
	QVector3D  vector3{};
	QVector4D  vector4{};
	QByteArray texture{};
};

static int const MAX_NUM_SKIN_BONES{24};

enum LightType
{
	Omnidirectional,
	Directional,
	Spot
};

static int const NUM_LODS{10};
static int const NUM_ALTS{10};

// Master vertex type, as stored in the files
#pragma pack(1)
struct MASTER_VERTEX
{
	QVector3D    position;
	QVector3D    normal;
	QVector2D    texCoord[4];
	QVector3D    tangent;
	QVector3D    binormal;
	QColor       color;
	QVector4D    unused;
	unsigned int boneIndices[4];
	float        boneWeights[4];
};
#pragma pack()

struct LightFieldSource
{
	enum Type
	{
		Point = 0,
		HCone,
		DualHCone,
		DualOpposedHCone,
		Line
	};
	enum Fade
	{
		Time = 0,
		Particles
	};

	Type          type;
	Fade          fadeType;
	QVector3D     position;
	QColor        diffuse;
	float         intensity;
	float         width;
	float         length;
	float         height;
	float         autoDestructTime;
	float         autoDestructFadeTime;
	float         intensityNoiseScale;
	float         intensityNoiseTimeScale;
	float         angularVelocity;
	bool          affectedByGlobalIntensity;
	unsigned long particles;
};

}
#endif
