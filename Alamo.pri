INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

SOURCES += \
	$$PWD/Animations.cpp \
	$$PWD/ChunkFile.cpp \
	$$PWD/Map.cpp \
	$$PWD/Models.cpp

HEADERS += \
	$$PWD/Animations.hpp \
	$$PWD/ChunkFile.hpp \
	$$PWD/Map.hpp \
	$$PWD/Models.hpp \
	$$PWD/gameTypes.hpp \
	$$PWD/utils.hpp
