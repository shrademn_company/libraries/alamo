//------------------------------------------------------------------------------
//
// Original source code: https://github.com/AlamoEngine-Tools/ModCheck
//
//------------------------------------------------------------------------------

#include <QDebug>

#include <core.tpp>

#include "ChunkFile.hpp"
#include "Map.hpp"

using namespace std;

namespace Alamo
{

Map::Map(QFileInfo const &file): m_file{file}
{
	try
	{
		ChunkFile reader{m_file};

		readProperties(reader);
		reader.next();
		if (reader.type() == 3)
			reader.next();
		if (reader.type() == 2)
		{
			reader.skip();
			reader.next();
		}
		if (reader.type() == 0x13)
		{
			m_preview = true;
			reader.skip();
			reader.next();
		}
		if (reader.type() == 3)
			reader.next();
		if (reader.type() != 1)
		{
			reader.skip();
			reader.next();
		}
		ChunkFile::verify(reader.type() == 1);
		readEnvironments(reader);
		if (m_properties.type == MAP_LAND)
		{
			readTerrain(reader);
			readPassability(reader);
		}
		readObjects(reader);
		if (m_properties.type == MAP_LAND)
			readZones(reader);
	}
	catch (const std::exception &exception)
	{
		m_error = exception.what();
	}
	if (!m_error.isEmpty())
		qWarning() << __FUNCTION__ << __LINE__ << m_error;
}

void Map::readProperties(ChunkFile &reader)
{
	ChunkFile::verify(reader.next() == 0);
	while (reader.nextMini() != -1)
	{
		if (reader.type() == 1)
			m_properties.type = (reader.readInteger() == MAP_SPACE ? MAP_SPACE : MAP_LAND);
		else if (reader.type() == 2)
			m_properties.players = reader.readInteger();
		else if (reader.type() == 8)
			m_properties.name = reader.readWideString();
		else if (reader.type() == 9)
			m_properties.planet = reader.readWideString();
		else if (reader.type() == 16)
			m_properties.dimension.setHeight(reader.readFloat());
		else if (reader.type() == 17)
			m_properties.dimension.setWidth(reader.readFloat());
	}
}

void Map::readEnvironments(ChunkFile &reader)
{
	ChunkFile::verify(reader.next() == ChunkEnvironment);
	ChunkFile::verify(reader.next() == 4);
	while (reader.next() == 6)
	{
		Environment environment;

		environment.position = reader.fileTell();
		environment.size = reader.size();
		while (reader.nextMini() != -1)
		{
			if (reader.type() == 0x14)
				environment.name = reader.readString();
			else if (reader.type() == 0x29)
				environment.scenario = reader.readString();
			else if (reader.type() == 0x19)
				environment.skybox1 = reader.readString();
			else if (reader.type() == 0x1A)
				environment.skybox2 = reader.readString();
			else if (reader.type() == 0x2F)
				environment.clouds = reader.readString();
		}
		m_environments << environment;
	}
	ChunkFile::verify(reader.type() == 5);
	ChunkFile::verify(reader.size() == 0); // Empty node, for some reason
	ChunkFile::verify(reader.next() == ChunkEnd); // End of environment list
	ChunkFile::verify(reader.next() == 2); // We don't care about chunk type 2 (copy of the active environment fog settings)
	ChunkFile::verify(reader.next() == 0);
	ChunkFile::verify(reader.next() == ChunkEnd);
	reader.next();
	if (reader.type() == 8)
		reader.next(); // Read active environment index
	ChunkFile::verify(reader.type() == ChunkEnd);
}

void Map::readLayer(ChunkFile &reader, Layer &layer)
{
	ChunkFile::verify(reader.next() == ChunkTerrainLayer);
	while (reader.nextMini() != -1)
	{
		if (reader.type() == 12)
			layer.colorTexture = reader.readString();
		else if (reader.type() == 19)
			layer.normalTexture = reader.readString();
	}
}

void Map::readDecals(ChunkFile &reader)
{
	while (reader.next() == ChunkTerrainDecal)
		;
	ChunkFile::verify(reader.type() == ChunkEnd);
}

void Map::readTracks(ChunkFile &reader)
{
	while (reader.next() == ChunkTerrainTrack)
	{
		ChunkFile::verify(reader.next() == ChunkTerrainTrackInformation);
		ChunkFile::verify(reader.next() == ChunkTerrainTrackTexture);
		m_tracks << Track{reader.readString()};
		ChunkFile::verify(reader.next() == ChunkTerrainTrackPoints);
		ChunkFile::verify(reader.next() == ChunkTerrainTrackWidths);
		ChunkFile::verify(reader.next() == ChunkTerrainTrackOpacities);
		ChunkFile::verify(reader.next() == ChunkEnd);
	}
	ChunkFile::verify(reader.type() == ChunkEnd);
}

void Map::readScrollInfo(ChunkFile &reader)
{
	ChunkFile::verify(reader.next() == 0x0aa45bcd);
	ChunkFile::verify(reader.next() == ChunkEnd);
}

void Map::readTerrain(ChunkFile &reader)
{
	ChunkFile::verify(reader.next() == ChunkTerrain);
	ChunkFile::verify(reader.next() == 0);
	while (reader.nextMini() != -1)
	{
		if (reader.type() == 0)
			m_data.size.setWidth(reader.readInteger());
		else if (reader.type() == 1)
			m_data.size.setHeight(reader.readInteger());
		else if (reader.type() == 5)
			m_layers.resize(reader.readInteger());
		else if (reader.type() == 29)
			m_water.maps[0] = reader.readString();
		else if (reader.type() == 30)
			m_water.maps[1] = reader.readString();
		else if (reader.type() == 31)
			m_water.maps[2] = reader.readString();
	}
	m_properties.dimension = (m_data.size - QSize{1, 1}) / 31;
	ChunkFile::verify(reader.next() == 7);
	readScrollInfo(reader);
	ChunkFile::verify(reader.next() == 8);
	readScrollInfo(reader);
	ChunkFile::verify(reader.next() == 5);
	m_data.elevationMap = reader.readData<int>(m_data.size.width() * m_data.size.height());
	for (auto &value: m_data.elevationMap)
	{
		value = static_cast<qint16>(value);
		m_data.maximumElevation = qMax(value, m_data.maximumElevation);
		m_data.minimumElevation = qMin(value, m_data.minimumElevation);
	}
	ChunkFile::verify(reader.next() == 2); // Read texture list
	for (auto &layer: m_layers)
		readLayer(reader, layer);
	ChunkFile::verify(reader.next() == ChunkEnd);
	ChunkFile::verify(reader.next() == 6);
	if (reader.size() > 0)
		readDecals(reader);
	ChunkFile::verify(reader.next() == 9);
	if (reader.size() > 0)
		readTracks(reader);
	ChunkFile::verify(reader.next() == ChunkEnd);
}

void Map::readPassability(ChunkFile &reader)
{
	ChunkFile::verify(reader.next() == ChunkPassability);
	m_data.passabilityMap = reader.readData<int>(m_data.size.width() * m_data.size.height());
}

void Map::readObject(ChunkFile &reader)
{
	Object object{0, 0};

	ChunkFile::verify(reader.next() == 0x454);
	ChunkFile::verify(reader.next() == 0x459);
	ChunkFile::verify(reader.next() == 0x4B0);
	while (reader.nextMini() != -1)
		if (reader.type() == 1)
			object.crc = reader.readInteger();
	reader.next();
	if (reader.type() == 0x4B9) // Unknown use; always empty
	{
		ChunkFile::verify(reader.size() == 0);
		reader.next();
	}
	if (reader.type() == 0x4BA) // Ability list
	{
		while (reader.next() == 0)
			;
		ChunkFile::verify(reader.type() == ChunkEnd);
		reader.next();
	}
	if (reader.type() == 0x4B7) // Unknown use
	{
		ChunkFile::verify(reader.next() == 0x4B8);
		ChunkFile::verify(reader.next() == ChunkEnd);
		reader.next();
	}
	ChunkFile::verify(reader.type() == ChunkEnd);
	reader.next();
	if (reader.type() == 0x46E) // Unknown use
	{
		ChunkFile::verify(reader.next() == 0);
		ChunkFile::verify(reader.next() == ChunkEnd);
		reader.next();
	}
	if (reader.type() == 0x482) // Chunk that contains the hint text (even if not used)
	{
		ChunkFile::verify(reader.next() == 0);
		ChunkFile::verify(reader.next() == ChunkEnd);
		reader.next();
	}
	if (reader.type() == 0x476) // Chunk that contains the CRC of the initial object (e.g, on build pads)
	{
		ChunkFile::verify(reader.next() == 1);
		while (reader.nextMini() != -1)
			if (reader.type() == 2)
				object.initialObject = reader.readInteger();
		ChunkFile::verify(reader.next() == ChunkEnd);
		reader.next();
	}
	if (reader.type() == 0x47a) // Unknown use
	{
		reader.skip();
		reader.next();
	}
	ChunkFile::verify(reader.type() == ChunkEnd);
	m_objects.push_back(object);
}

void Map::readObjects(ChunkFile &reader)
{
	ChunkFile::verify(reader.next() == ChunkObject);
	ChunkFile::verify(reader.next() == 0);
	ChunkFile::verify(reader.next() == 1);
	if (reader.size() > 0)
		while (reader.next() != -1)
		{
			if (reader.type() == 0x44C)
				readObject(reader);
			else
				reader.skip();
		}
}

void Map::readZones(ChunkFile &reader)
{
	ChunkFile::verify(reader.next() == 8);
	ChunkFile::verify(reader.next() == ChunkEnd);
	ChunkFile::verify(reader.next() == ChunkZoneInformation);
	ChunkFile::verify(reader.nextMini() == 5);
	m_data.playableTop.setY(reader.readFloat());
	m_data.playableBottom.setX(reader.readFloat());
	ChunkFile::verify(reader.nextMini() == 6);
	m_data.playableBottom.setY(reader.readFloat());
	m_data.playableTop.setX(reader.readFloat());
	m_data.playableTop.setX(m_data.size.width() * 20.F - m_data.playableTop.x());
	m_data.playableBottom.setX(m_data.size.width() * 20.F - m_data.playableBottom.x());
	ChunkFile::verify(reader.nextMini() == -1);
	ChunkFile::verify(reader.next() == ChunkZoneData);
	if (reader.size())
	{
		QMap<QPair<int, int>, int> connections;
		QMap<int, QByteArray>      names;

		m_data.zoneMap.resize(m_data.size.width() * m_data.size.height());
		while (reader.next() == 0)
		{
			QByteArray      name;
			QPair<int, int> zones{0, 0};

			ChunkFile::verify(reader.next() == 0);
			while (reader.nextMini() != -1)
			{
				if (reader.type() == 1)
					name = reader.readString();
				else if (reader.type() == 2)
					zones.first = reader.readInteger();
			}
			names[zones.first] = name;
			ChunkFile::verify(reader.type() == ChunkEnd);
			ChunkFile::verify(reader.next() == 14);
			while (reader.next() == 0)
			{
				ChunkFile::verify(reader.nextMini() == 1);
				zones.second = reader.readInteger();
				ChunkFile::verify(reader.nextMini() == 2);
				connections[zones] = reader.readInteger();
				ChunkFile::verify(reader.nextMini() == -1);
			}
			if (reader.type() != 7)
				ChunkFile::verify(reader.next() == 7);
			while (reader.next() == 0)
			{
				int x{0};
				int y{0};

				ChunkFile::verify(reader.nextMini() == 1);
				y = reader.readShort();
				x = reader.readShort();
				ChunkFile::verify(reader.nextMini() == 2);
				reader.readByte();
				m_data.zoneMap[x + y * m_data.size.height()] = zones.first;
				ChunkFile::verify(reader.nextMini() == -1);
			}
			ChunkFile::verify(reader.next() == 15);
			ChunkFile::verify(reader.next() == ChunkEnd);
		}
		if (!connections.isEmpty() && !checkConnections(connections, names))
			m_ai = QStringLiteral("Broken AI Data");
	}
	else
		m_ai = QStringLiteral("No AI Data");
}

QMap<QPair<int, int>, QList<int>> Map::rebuildZoneConnections() const
{
	QMap<QPair<int, int>, QMap<int, int>> connections;
	QMap<QPair<int, int>, QList<int>>     result;
	auto                                  checkConnection{[this, &connections](int from, int to)
		{
			QPair<int, int> zones{m_data.zoneMap[from], m_data.zoneMap[to]};

			if (zones.first > 0 && zones.second > 0 && zones.first != zones.second)
				++connections[zones][m_data.passabilityMap[to]];
		}};

	for (int index = 0; index < m_data.zoneMap.size(); ++index)
	{
		int x{index % m_data.size.width()};
		int y{index / m_data.size.width()};

		if (x < m_data.size.width() - 1 && y < m_data.size.height())
		{
			checkConnection(index, index + 1);
			checkConnection(index + 1, index);
		}
		if (x > 0 && y < m_data.size.height() - 1)
		{
			checkConnection(index, index - 1 + m_data.size.width());
			checkConnection(index - 1 + m_data.size.width(), index);
		}
		if (x < m_data.size.width() && y < m_data.size.height() - 1)
		{
			checkConnection(index, index + m_data.size.width());
			checkConnection(index + m_data.size.width(), index);
		}
		if (x < m_data.size.width() - 1 && y < m_data.size.height() - 1)
		{
			checkConnection(index, index + 1 + m_data.size.width());
			checkConnection(index + 1 + m_data.size.width(), index);
		}
	}
	for (auto const &&[zones, passabilityCounts]: keyValues(connections))
		result[zones] = passabilityCounts.keys(*std::max_element(passabilityCounts.begin(), passabilityCounts.end()));
	return result;
}

bool Map::checkConnections(const QMap<QPair<int, int>, int> &connections, const QMap<int, QByteArray> &names)
{
	auto rebuiltConnections{rebuildZoneConnections()};

	if (adapt(connections.keys()) != adapt(rebuiltConnections.keys()))
	{
		for (auto const &zones: keys(connections))
			if (!rebuiltConnections.contains(zones))
				qDebug() << __FUNCTION__ << __LINE__ << m_file << zones << names[zones.first] << names[zones.second];
		for (auto const &zones: keys(rebuiltConnections))
			if (!connections.contains(zones))
				qDebug() << __FUNCTION__ << __LINE__ << m_file << zones << names[zones.first] << names[zones.second];
		return false;
	}
	for (auto const &&[zones, passability]: keyValues(rebuiltConnections))
		if (!passability.contains(connections[zones]))
		{
			qDebug() << __FUNCTION__ << __LINE__ << m_file << zones << names[zones.first] << names[zones.second] << passability;
			return false;
		}
	return true;
}

QString Map::toString(QList<int> const &map, int width)
{
	QString result;

	result.reserve(map.size() * 2 + map.size() / width);
	for (int index = 0; index < map.size(); ++index)
	{
		if (index % width)
			result += "\t";
		result += QString::number(map[index]);
		if (index % width == width - 1)
			result.append(QChar::LineFeed);
	}
	return result;
}

}
