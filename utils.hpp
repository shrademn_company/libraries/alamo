//------------------------------------------------------------------------------
//
// Original source code: https://github.com/GlyphXTools/alo-viewer
//
//------------------------------------------------------------------------------

#ifndef UTILS_H
#define UTILS_H

#include <QDir>
#include <QFileInfo>
#include <stddef.h>

#include "Animations.hpp"
#include "Models.hpp"

namespace Alamo
{

inline unsigned long CRC32(char const *data, size_t size)
{
	static unsigned long lookupTable[256];
	static bool          validTable{false};
	unsigned long        crc{0xFFFFFFFF};

	if (!validTable)
	{
		for (int i = 0; i < 256; i++)
		{
			unsigned long crc = i;
			for (int j = 0; j < 8; j++)
				crc = (crc & 1) ? (crc >> 1) ^ 0xEDB88320 : (crc >> 1);
			lookupTable[i] = crc & 0xFFFFFFFF;
		}
		validTable = true;
	}
	for (size_t j = 0; j < size; j++)
		crc = ((crc >> 8) & 0x00FFFFFF) ^ lookupTable[(crc ^ data[j]) & 0xFF];
	return crc ^ 0xFFFFFFFF;
}

inline QFileInfoList getAnimationFiles(QFileInfo const &model)
{
	return model.dir().entryInfoList({model.completeBaseName() + "_*.ALA"});
}

inline void scale(Model &model, double scale)
{
	if (scale == 1)
		return;
	for (auto &bone: model.bones())
		bone.relativeTransform.setColumn(3, bone.relativeTransform.column(3) * scale);
	for (auto &mesh: model.meshes())
		for (auto &subMesh: mesh.subMeshes)
			for (auto &vertex: subMesh.vertices)
				vertex.position *= scale;
}

inline void scale(Animation &animation, double scale)
{
	if (scale == 1)
		return;
	for (auto &bone: animation.bones())
	{
		bone.ofsTranslation *= scale;
		bone.scaleTranslatioon *= scale;
	}
}

}

#endif
