//------------------------------------------------------------------------------
//
// Original source code: https://github.com/GlyphXTools/alo-viewer
//
//------------------------------------------------------------------------------

#ifndef ANIMATIONS_H
#define ANIMATIONS_H

#include "Models.hpp"

namespace Alamo
{

class Animation
{
public:
	enum Chunk
	{
		ChunkAnimation                = 0x1000,
		ChunkAnimationInformation     = 0x1001,
		ChunkAnimationTranslationData = 0x100A,
		ChunkAnimationRotationData    = 0x1009,
		ChunkBone                     = 0x1002,
		ChunkBoneInformation          = 0x1003,
		ChunkBoneTranslationData      = 0x1004,
		ChunkBoneScaleData            = 0x1005,
		ChunkBoneRotationData         = 0x1006,
		ChunkBoneVisibilityData       = 0x1007,
		ChunkEnd                      = -1
	};

	struct Bone
	{
		int                  index;
		QVector3D            ofsTranslation;
		QVector3D            scaleTranslatioon;
		QVector3D            ofsScale;
		QVector3D            scaleScale;
		unsigned short       indexTranslation{UINT16_MAX};
		unsigned short       indexScale{UINT16_MAX};
		unsigned short       indexRotation{UINT16_MAX};
		QQuaternion          defRotation;
		QList<unsigned char> visibilityData;
	};
	struct Frame
	{
		QQuaternion rotation;
		QVector3D   translation;
		QVector3D   scale;
		bool        visible{true};
	};

	Animation(QFileInfo const &file, Model const &model);
	~Animation() = default;

	inline QString const &name() const
	{
		return m_name;
	}

	inline QString const &suffix() const
	{
		return m_suffix;
	}

	constexpr float fps() const
	{
		return m_fps;
	}

	constexpr size_t frameCount() const
	{
		return m_frameCount;
	}

	inline QList<Bone> const &bones() const
	{
		return m_bones;
	}

	inline QList<Bone> &bones()
	{
		return m_bones;
	}

	Frame const &getFrameInfo(size_t bone, size_t frame) const;
	bool        isVisible(size_t bone, float time) const;

	// Returns the first time in [from,to] where the bone was (in)visible.
	// Returns -1 if there is no such time.
	float getVisibleEvent(size_t bone, float from, float to) const;
	float getInvisibleEvent(size_t bone, float from, float to) const;
	int   getVisibleFrame(size_t bone, size_t from, size_t to) const;
	int   getInvisibleFrame(size_t bone, size_t from, size_t to) const;

	void save(QFileInfo const &file, Model const &model);

	constexpr QString const &error() const
	{
		return m_error;
	}

private:
	struct PackedQuaternion
	{
		int16_t x;
		int16_t y;
		int16_t z;
		int16_t w;

		PackedQuaternion() = default;

		inline PackedQuaternion(ChunkFile &reader)
		{
			x = reader.readShort();
			y = reader.readShort();
			z = reader.readShort();
			w = reader.readShort();
		}

		inline PackedQuaternion(QQuaternion const &quaternion)
		{
			x = quaternion.x() * static_cast<float>(INT16_MAX);
			y = quaternion.y() * static_cast<float>(INT16_MAX);
			z = quaternion.z() * static_cast<float>(INT16_MAX);
			w = quaternion.scalar() * static_cast<float>(INT16_MAX);
		}

		inline QQuaternion unpack() const
		{
			return {w / static_cast<float>(INT16_MAX), x / static_cast<float>(INT16_MAX), y / static_cast<float>(INT16_MAX), z / static_cast<float>(INT16_MAX)};
		}
	};

	struct PackedVector
	{
		int16_t x;
		int16_t y;
		int16_t z;

		PackedVector() = default;

		inline PackedVector(ChunkFile &reader)
		{
			x = reader.readShort();
			y = reader.readShort();
			z = reader.readShort();
		}

		inline QVector3D unpack() const
		{
			return {static_cast<float>(x), static_cast<float>(y), static_cast<float>(z)};
		}
	};
	struct AnimationData
	{
		size_t                  translationBlockSize;
		size_t                  rotationBlockSize;
		size_t                  scaleBlockSize;
		QList<PackedVector>     translationData;
		QList<PackedVector>     scaleData;
		QList<PackedQuaternion> rotationData;
	};

	void readAnimation(ChunkFile &reader, Model const &model);
	void readBone(ChunkFile &reader, Model const &model, Bone &bone);

	void constructTransforms(Model const &model);

	void writeAnimation(ChunkFile &writer, Model const &model);
	void writeBone(ChunkFile &writer, QByteArray const &name, Bone const &bone);

	QList<Bone> computeBones(QList<int> const &boneIndexes) const;

	QString       m_name;
	QString       m_suffix;
	float         m_fps;
	size_t        m_frameCount;
	QList<Bone>   m_bones;
	AnimationData m_data;
	QList<Frame>  m_frames;
	QString       m_error;
};

}

#endif
